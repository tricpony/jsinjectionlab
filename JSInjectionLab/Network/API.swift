//
//  API.swift
//  JSInjectionLab
//
//  Created by aarthur on 3/3/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import Foundation

struct API {
    static let endPoint = "https://jumboassetsv1.blob.core.windows.net/publicfiles/interview_bundle.js"
}
