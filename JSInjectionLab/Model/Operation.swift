//
//  Operation.swift
//  JSInjectionLab
//
//  Created by aarthur on 3/4/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import Foundation

/// Class representing a javascript operation.
class Operation: Decodable, Equatable {
    var id = ""
    var message = ""
    var state: String?
    var progress: Int? = nil {
        didSet {
            guard let delegate = delegate else {
                return
            }
            // alert the delegate that progress has changed.
            delegate.operationDidChange(to: self)
        }
    }
    weak var delegate: OperationObserver?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case message
        case state
        case progress
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        message = try values.decode(String.self, forKey: .message)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        progress = try values.decodeIfPresent(Int.self, forKey: .progress)
    }

    /// Update self.
    /// - Parameters:
    ///   - freshOperation: Operation to sync self to.
    func update(freshOperation: Operation?) {
        guard let freshOperation = freshOperation else {
            return
        }
        id = freshOperation.id
        message = freshOperation.message
        state = freshOperation.state
        progress = freshOperation.progress
    }
    
    // MARK: Equatable
    
    static func ==(lhs: Operation, rhs: Operation) -> Bool {
        return lhs.id == rhs.id
    }
}
