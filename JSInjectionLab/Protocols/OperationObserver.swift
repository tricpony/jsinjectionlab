//
//  OperationObserver.swift
//  JSInjectionLab
//
//  Created by aarthur on 3/4/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit

/// Protocol that will be triggered when an operation progress changes.
protocol OperationObserver where Self: UITableViewCell {
    /// Update cell content.
    /// - Parameters:
    ///   - to: Operation that needs to be sync'ed to.
    func operationDidChange(to: Operation)
}
