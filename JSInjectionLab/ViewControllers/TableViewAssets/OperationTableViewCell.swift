//
//  OperationTableViewCell.swift
//  JSInjectionLab
//
//  Created by aarthur on 3/4/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit

class OperationTableViewCell: UITableViewCell, OperationObserver {
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var statusLabel: UILabel!
    static let reuseIdentifier = "OperationTableViewCell"
    
    func fillCell(_ operation: Operation, firstFill: Bool = true) {
        statusLabel.text = operation.message
        statusLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        guard let progress = operation.progress else {
            guard let state = operation.state else {
                return
            }
            switch state {
            case "success":
                progressBar.progress = 1
            default:
                statusLabel.text = state + " on ID: \(operation.id)"
                statusLabel.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            }
            return
        }
        progressBar.progress = Float(progress) / 100.0
        if firstFill {
            operation.delegate = self
        }
    }
    
    // MARK: OperationObserver
    
    func operationDidChange(to: Operation) {
        fillCell(to, firstFill: false)
    }
}
