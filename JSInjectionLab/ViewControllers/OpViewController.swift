//
//  OpViewController.swift
//  JSInjectionLab
//
//  Created by aarthur on 3/3/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit
import WebKit

/// Displays state and progress of all operations that have been launched.
class OpViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pinWheel: UIActivityIndicatorView!
    var javascript = ""
    var webView: WKWebView!
    var operationId = 1000
    var dataSource: [Operation] = [Operation]() {
        didSet {
            // reload table when items are added or removed from array.
            tableView.reloadData()
        }
    }

    /// Load left and right navigation buttons.
    func loadNavButtons() {
        let rtNavItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(OpViewController.performJSOperation))
        let ltNavItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(OpViewController.clearOperations))
        // add new operations
        navigationItem.rightBarButtonItem = rtNavItem
        // clear all operations
        navigationItem.leftBarButtonItem = ltNavItem
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pinWheel.startAnimating()
        fetchJavascript()
        updateTitle()
    }
    
    /// Update view controller title.
    /// - Parameters:
    ///   - fromDataSource: When true include dataSource count.
    func updateTitle(fromDataSource: Bool = false) {
        if fromDataSource == false {
            title = "Add Operation"
        } else {
            title = "In Flight: \(dataSource.count)"
        }
    }
    
    func cacheWords() {
        let searchPattern = "\\b[a-z0-9]+\\b"
        let regex = try! NSRegularExpression(pattern: searchPattern, options: .caseInsensitive)
        var wordCount = [Substring: Int]()
        let matches = regex.matches(in: javascript, options: [], range: NSRange(location: 0, length: javascript.count))
        wordCount.reserveCapacity(matches.count)
        for match in matches {
            let lowerBound = javascript.index(javascript.startIndex, offsetBy: match.range.location)
            let upperBound = javascript.index(lowerBound, offsetBy: match.range.length)
            let word = javascript[lowerBound..<upperBound]
            let countSoFar = wordCount[word] ?? 0
            wordCount[word] = countSoFar + 1
        }
        findMaxWord(wordCount: wordCount)
        listOrderedWords(wordCount: wordCount)
        sumTotalWords(wordCount: wordCount)
    }
    func findMaxWord(wordCount: [Substring: Int]) {
        let highCount = wordCount.max { a, b in a.value < b.value }
        let word = highCount?.key ?? "Word not found"
        let count = highCount?.value ?? 0
        print("Word: '\(word)' Count: \(count)\n")
    }
    func listOrderedWords(wordCount: [Substring: Int]) {
        let sortedByValueDictionary = wordCount.sorted { $0.1 > $1.1 }
        let _ = sortedByValueDictionary.map { print("Word: '\($0.0)' Count: \($0.1)") }
    }
    func sumTotalWords(wordCount: [Substring: Int]) {
        let total = wordCount.compactMap { $0.1 }.reduce(0, +)
        print("\nTotal: '\(total)'")
    }
    
    // MARK: Service Call
    
    /// Execute service to download javascript source.
    func fetchJavascript() {
        let _ = ServiceManager.sharedService.startServiceAt(urlString: API.endPoint) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let data):
                strongSelf.javascript = String(decoding: data, as: UTF8.self)
                DispatchQueue.main.async {
                    strongSelf.injectJavascript()
                    strongSelf.cacheWords()
                }
            case .failure( _ ):
                DispatchQueue.main.async {
                    strongSelf.pinWheel.stopAnimating()
                    // Error occurred, alert the user
                    let alertController = UIAlertController(title: "Alert", message: "Service Failed", preferredStyle: .alert)
                    let buttonAction = UIAlertAction(title: "OK", style: .default, handler:nil)
                    alertController.addAction(buttonAction)
                    strongSelf.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK: Target / Action
    
    /// Call js function startOperation.
    @objc
    func performJSOperation() {
        let arg = String(operationId)
        let js = "startOperation('\(arg)');"
        webView.evaluateJavaScript(js) { [weak self] (data, error) in
            guard let strongSelf = self else { return }
            guard let error = error else {
                strongSelf.operationId += 1
                return
            }
            // Error occurred, alert the user
            let alertController = UIAlertController(title: "Alert", message: error.localizedDescription, preferredStyle: .alert)
            let buttonAction = UIAlertAction(title: "OK", style: .default, handler:nil)
            alertController.addAction(buttonAction)
            strongSelf.present(alertController, animated: true, completion: nil)
        }
    }
    
    /// Clear operations once they are all complete.
    @objc
    func clearOperations() {
        let allow = dataSource.filter { $0.message == "progress" }.isEmpty
        if allow {
            updateTitle()
            dataSource = [Operation]()
        }
    }
    
    // MARK: Javascript

    /// Inject javascript into web view.
    func injectJavascript() {
        let configuration = WKWebViewConfiguration()
        let userContentController = WKUserContentController()
        let userScript = WKUserScript(source: javascript,
                                      injectionTime: .atDocumentStart,
                                      forMainFrameOnly: false)
        userContentController.addUserScript(userScript)
        userContentController.add(self, name:"jumbo")
        configuration.userContentController = userContentController
        webView = WKWebView(frame: .zero, configuration: configuration)
        webView.navigationDelegate = self
        view.addSubview(webView)
        webView.loadHTMLString("http://apple.com", baseURL: nil)
    }

}

extension OpViewController: WKScriptMessageHandler, WKNavigationDelegate {
    /// Convert json string to data object.
    /// - Parameters:
    ///   - json: json as a string.
    /// - Returns: Optional data object that was converted from json.
    func encode(json: String) -> Data? {
        guard let jsonData = json.data(using: .utf8) else {
            return nil
        }
        return jsonData
    }
    
    // MARK: WKScriptMessageHandler
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        guard let response = message.body as? String else { return }
        guard let data = encode(json: response) else { return }
        do {
            let decoder = JSONDecoder()
            let operation = try decoder.decode(Operation.self, from: data)
            if dataSource.contains(operation) == false {
                dataSource.append(operation)
            } else {
                let staleOp = dataSource.filter { $0.id == operation.id }.first
                staleOp?.update(freshOperation: operation)
            }
            updateTitle(fromDataSource: true)
        } catch {
            fatalError()
        }
    }
    
    // MARK: WKNavigationDelegate

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadNavButtons()
        pinWheel.stopAnimating()
    }
}

extension OpViewController: UITableViewDelegate, UITableViewDataSource {

    // MARK: - Table View

    func cellIdentifier(at indexPath: IndexPath) -> String {
        return OperationTableViewCell.reuseIdentifier
    }
    
    func nextCellForTableView(_ tableView: UITableView, at indexPath: IndexPath) -> OperationTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier(at: indexPath)) as? OperationTableViewCell else {
            fatalError("Cell identifier not found.")
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.nextCellForTableView(tableView, at: indexPath)
        let operation = dataSource[indexPath.row]
        cell.fillCell(operation)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


