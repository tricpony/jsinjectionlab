3-4-2020

This is an iOS app that I wrote to apply for an opening with a company named Jumbo.  The project is written in swift.

Key ingredients are:
  1 one view controller
  2 one model class
  3 network service manager

The requirements for the project are at https://join.jumboprivacy.com/20191218ios.html.  According to these requirements, specified javascript source is downloaded at launch in (1) by (3).  This source is injected into a web view and a function named startOperation(id) is called by tapping on a + button on the navigation bar of (1).  Each operation is represented by a table cell in a table view in (1).  Each cell contains a progress bar reflecting the status of its operation.  Progress is updated on the cell by observing the progress property in the model class (2).